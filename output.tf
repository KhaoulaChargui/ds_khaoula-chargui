data "azurerm_public_ip" "azuretp1" {
  name                = "${var.name}-public-ip"
  resource_group_name = "${var.name}-rg"
}


output "public_ip" {
  value = data.azurerm_public_ip.azuretp1.ip_address
}